import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';


class Refresh extends Component {
    handleClick() {
        const { refresh } = this.props;
        refresh();
    }

    render() {
        return (
            <div className="refresh" onClick={() => this.handleClick()}></div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Refresh);