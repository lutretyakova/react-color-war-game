import React, { Component } from 'react';

import Refresh from './Refresh';
import Leaders from './Leaders';
import Info from './Info';

export default class ControlPnl extends Component {
    render() {
        return (
            <div className="control-buttons">
                <Refresh />
                <Leaders />
                <Info />
            </div>
        )
    }
}