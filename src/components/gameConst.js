export const HEIGHT_MAX = 30;

export const WIDTH_MAX = 20;

export const CELL_WIDTH_MIN = 20;

export const CELL_DESKTOP = 40;

export const CELL_MOBILE = 20;

export const COLOR_COUNT = 6;

export const COLORS = [
    'coral', 'yellow', 'lawngreen', 'cyan', 'silver', 'burlywood'
]

export const PLAYER = 1;

export const COMPUTER = 2;

export const NOBODY = 0;


export const DESKTOP_SIZE_MAX = 1000;

export const DESKTOP_SIZE_MIN = 600;


export const MODAL_NULL = '';

export const MODAL_WIN = 'Поздравляем с победой!';

export const MODAL_LOSE = 'К сожалению, вы проиграли.';

export const MODAL_STATISTIC = 'Статистика';

export const MODAL_INFO = 'Ссылка на исходный код';

export const GIT_LINK = 'https://gitlab.com/lutretyakova/react-color-war-game';
