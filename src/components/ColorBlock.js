import React, {Component} from 'react';

import {connect} from 'react-redux';

import * as actions from '../actions';

class ColorBlock extends Component{
    handleClick(colorCode){
       const { selectColor } = this.props;
       selectColor(colorCode); 
    }


    render(){
        const { color, state, code } = this.props;
        const blockStyle = {
            'backgroundColor': color
        }
        const activeFlag = code === state.playerColor ? 'active player': '';

        const compColor = code === state.compColor ? 'computer disabled': '';
        return(
            <div style={blockStyle} className={`color-bl ${activeFlag} ${compColor}`} 
                    onClick={() => this.handleClick(code)}>
            </div>
        )
    }
}

export default connect((state) => ({state: state}), actions)(ColorBlock);