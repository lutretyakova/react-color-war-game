import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../actions';
import { GIT_LINK } from './gameConst';


class Modal extends Component {
    handleClick(){
        const { closeModal } = this.props;
        closeModal();
    }


    render() {
        const { header, text } = this.props;

        let pageHtml = null;
        if (text !== undefined) {
            if (text === GIT_LINK) {
                pageHtml =
                    <div className='modal-link' onClick={() => this.handleClick()}>
                        <a href={text}>{text}</a>
                    </div>
            } else {
                pageHtml =
                    <div>
                        <div className='modal-context header'>
                            <div>#</div>
                            <div>Дата игры</div>
                            <div>Победитель</div>
                        </div>
                        <div className='modal-context' onClick={() => this.handleClick()}>
                            {text.map(data => (data.map((item, inx) => (<div key={inx}>{item}</div>))))}
                        </div>
                    </div>
            }
        }
        return (
            <div className='modal' onClick={() => this.handleClick()}>
                <h1>{header}</h1>
                {pageHtml}
            </div>
        )
    }
}


export default connect((state) => ({ state: state }), actions)(Modal);