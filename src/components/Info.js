import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

class Info extends Component{
    handleClick(){
        const { info } = this.props;
        info();
    }

    render(){
        return(
            <div className='info' onClick = {() => this.handleClick()}></div>
        )
    }
}


export default connect((state) => ({state: state}), actions)(Info);