import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import {COLORS, PLAYER, COMPUTER} from './gameConst';

class Cell extends Component {
    numberToGridInx(number, WIDTH){
        const y = Math.floor(number / WIDTH);
        const x = number % WIDTH;
        return {y, x}
    }


    render() {
        const { state, number } = this.props;
        const {y, x} = this.numberToGridInx(number, state.WIDTH);
        const cellStyle = {
            'width': `${state.CELL_WIDTH}px`,
            'height': `${state.CELL_WIDTH}px`,
            'backgroundColor': COLORS[state.grid[y][x]]
        }
        const playerClass = state.gameBoard[y][x] === PLAYER ? 'player':
                            state.gameBoard[y][x] === COMPUTER ? "computer":
                            '';
        return (
            <div style={cellStyle} className={`cell ${playerClass}`}></div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Cell);