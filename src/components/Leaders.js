import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../actions';

class Leaders extends Component{
    handleClick(){
        const { statistic } = this.props;
        statistic();
    }

    render(){
        return(
            <div className='show-leaders' onClick={() => this.handleClick()}></div>
        )
    }
}

export default connect((state) => ({state: state }), actions)(Leaders);