import React, { Component } from 'react';
import './ColorWar.css';

import Cell from './components/Cell';

import { connect } from 'react-redux';
import * as actions from './actions';

import { COLORS, DESKTOP_SIZE_MAX, DESKTOP_SIZE_MIN, MODAL_NULL } from './components/gameConst';
import ColorBlock from './components/ColorBlock';
import ControlPnl from './components/ControlPnl';
import Modal from './components/Modal';

class ColorWar extends Component {
    isDesktop(width, height){
        return (width < DESKTOP_SIZE_MAX && width > DESKTOP_SIZE_MIN) ||
        (height < DESKTOP_SIZE_MAX && height > DESKTOP_SIZE_MIN)
    }

    constructor(){
        super();
        this.handleResize = this.handleResize.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleResize);
        const width = document.getElementById('board').offsetWidth;
        const height = window.innerHeight;
            
        const { setBoardSize } = this.props;
        setBoardSize(width, height);
    } 

    componentWillUnmount() {
        window.addEventListener("resize", null);
    }

    handleResize() {
        const width = document.getElementById('board').clientWidth;
        const height = window.innerHeight;

        const { resize } = this.props;
        resize(width, height);
    }

    render() {
        const { state } = this.props;
        let container = <div> Подождите </div>;

        if (state.WIDTH !== undefined && state.HEIGHT !== undefined) {
            const WIDTH = state.WIDTH;
            const HEIGHT = state.HEIGHT;

            let cells = [];
            for (let inx = 0; inx < HEIGHT * WIDTH; inx++) {
                cells.push(<Cell key={inx} number={inx} />)
            }
            const gridStyle = {
                'gridTemplateColumns': `repeat(${WIDTH}, ${state.CELL_WIDTH}px)`,
                'maxWidth': `${WIDTH * state.CELL_WIDTH}px`
            }
            container =
                <div style={gridStyle} className="grid">
                    {cells}
                </div>  
                
        }
            
        let colorBlocks = COLORS.map((color, inx) => <ColorBlock key={inx} code={inx} color={color} />)
        
        const modal = state.modal === MODAL_NULL ? null: 
                                <Modal header={state.modal} text={state.modalText}/>;
        
        return (
            <div className="container">
                 <div className="header" id="header">
                    <h1 className="title">ColorWar</h1>
                    <div className="color-blocks">
                        {colorBlocks}
                    </div>
                    <ControlPnl />
                </div>
                <div className="game-board" id="board">
                    { container }
                </div>
                { modal }
            </div>
        );
    }
}


export default connect((state) => ({ state: state }), actions)(ColorWar);
