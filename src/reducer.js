import {
    COLOR_COUNT, PLAYER, COMPUTER, NOBODY,
    WIDTH_MAX, HEIGHT_MAX, CELL_WIDTH_MIN,
    DESKTOP_SIZE_MIN, CELL_MOBILE, CELL_DESKTOP,
    MODAL_NULL,
    MODAL_WIN,
    MODAL_LOSE,
    MODAL_STATISTIC,
    MODAL_INFO,
    GIT_LINK
} from './components/gameConst';
import { SELECT_COLOR, SET_SIZE, RESIZE, REFRESH_GAME, STATICTIC, INFO, CLOSE_MODAL } from './actionsConst';
import { takeCellsOneColor, computerMove, defineWinner } from './gameLogic';
import { saveWinner, getWinners } from './statistic';

const setBoardSize = (state, width, height) => {
    let CELL_WIDTH = CELL_DESKTOP;
    if (width <= DESKTOP_SIZE_MIN) {
        CELL_WIDTH = CELL_MOBILE;
    }
    const WIDTH = Math.min(WIDTH_MAX, Math.floor(width / CELL_WIDTH));
    const HEIGHT = Math.min(HEIGHT_MAX, Math.floor(height / CELL_WIDTH));

    return {
        ...state,
        WIDTH: WIDTH,
        HEIGHT: HEIGHT,
        CELL_WIDTH: CELL_WIDTH
    }
}


const selectColor = (state, colorCode) => {
    const playerColor = state.currentColor === colorCode ? state.playerColorColor : colorCode;
    if (playerColor === state.currentColor) {
        return state
    }
    let gameBoard = state.gameBoard.map(row => row.slice());
    let grid = state.grid.map(row => row.slice());

    let playerCount;
    [gameBoard, grid, playerCount] = takeCellsOneColor(gameBoard,
        grid, playerColor,
        state.WIDTH, state.HEIGHT,
        PLAYER);

    let compColor, compCount;
    [gameBoard, grid, compColor, compCount] = computerMove(gameBoard, grid, playerColor, state.WIDTH, state.HEIGHT);

    const winner = defineWinner(playerCount, compCount, state.WIDTH, state.HEIGHT);
    let modal = MODAL_NULL;
    if (winner !== NOBODY) {
        saveWinner(winner);
        modal = winner === PLAYER ? MODAL_WIN: MODAL_LOSE;
    }
    return {
        ...state,
        grid: grid,
        gameBoard: gameBoard,
        playerColor: playerColor,
        compColor: compColor,
        modal: modal,
        modalText: undefined
    }
}


const resizeBoard = (state, newWidth, newHeight) => {
    const countedWidth = state.CELL_WIDTH * state.WIDTH;
    const countedHeight = state.CELL_WIDTH * state.HEIGHT;
    if (countedHeight !== newHeight || countedWidth !== newWidth) {
        const cell_width = Math.floor(newWidth / state.WIDTH);
        const cell_height = Math.floor(newHeight / state.HEIGHT);

        const cell_size = Math.min(cell_height, cell_width);

        return {
            ...state,
            CELL_WIDTH: Math.max(cell_size, CELL_WIDTH_MIN)
        }
    }

    return state
}


const newGame = (state) => {
    const WIDTH = state.WIDTH;
    const HEIGHT = state.HEIGHT;
    let grid = (new Array(HEIGHT)).fill(null);
    grid = grid.map(() => (new Array(WIDTH)).fill(null));

    for (let x = 0; x < WIDTH; x++) {
        for (let y = 0; y < HEIGHT; y++) {
            grid[y][x] = Math.floor(Math.random() * COLOR_COUNT);
        }
    }
    while (grid[0][0] === grid[HEIGHT - 1][WIDTH - 1]) {
        grid[0][0] = Math.floor(Math.random() * COLOR_COUNT);
    }

    let gameBoard = (new Array(HEIGHT)).fill(null);
    gameBoard = gameBoard.map(() => (new Array(WIDTH)).fill(NOBODY));

    gameBoard[0][0] = PLAYER;
    const playerColor = grid[0][0];
    [gameBoard, grid] = takeCellsOneColor(gameBoard, grid, playerColor,
                                            state.WIDTH, state.HEIGHT, PLAYER);
    const compColor = grid[HEIGHT - 1][WIDTH - 1];
    gameBoard[HEIGHT - 1][WIDTH - 1] = COMPUTER;
    [gameBoard, grid] = takeCellsOneColor(gameBoard, grid, compColor,
                                            state.WIDTH, state.HEIGHT, COMPUTER);

    return {
        ...state,
        grid: grid,
        gameBoard: gameBoard,
        compColor: grid[HEIGHT - 1][WIDTH - 1],
        playerColor: playerColor,
        modal: MODAL_NULL
    }
}


const showStatistic = (state, isShow) => {
    return{
        ...state,
        modal: MODAL_STATISTIC,
        modalText: getWinners()
    }
}


const getInfo = (state) => {
    return{
        ...state,
        modal: MODAL_INFO,
        modalText: GIT_LINK
    }
}


const closeModal = (state) => {
    return{
        ...state,
        modal: MODAL_NULL,
        modalText: undefined
    }
}

export default (state = {}, action) => {
    switch (action.type) {
        case SELECT_COLOR:
            return selectColor(state, action.color);
        case SET_SIZE:
            return newGame(setBoardSize(state, action.width, action.height));
        case RESIZE:
            return resizeBoard(state, action.width, action.height);
        case REFRESH_GAME:
            return newGame(state);
        case STATICTIC:
            return showStatistic(state);
        case INFO:
            return getInfo(state);
        case CLOSE_MODAL:
            return closeModal(state);
        default:
            return state;
    }
}