import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reducer from './reducer';
import './index.css';
import ColorWar from './ColorWar';
import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <ColorWar />
    </Provider>, document.getElementById('root'));
registerServiceWorker();
