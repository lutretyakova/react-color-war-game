export const SELECT_COLOR = 'select';

export const SET_SIZE = 'boardsize';

export const RESIZE = 'resize';

export const REFRESH_GAME = 'refresh';

export const STATICTIC = 'statistic';

export const INFO = 'info';

export const CLOSE_MODAL = 'close';