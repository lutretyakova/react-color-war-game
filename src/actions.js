import { SELECT_COLOR, SET_SIZE, RESIZE, REFRESH_GAME, STATICTIC, INFO, CLOSE_MODAL } from "./actionsConst";

export const selectColor = (color) => ({
    type: SELECT_COLOR,
    color: color
});

export const setBoardSize = (widthBoard, heightBoard) =>({
    type: SET_SIZE,
    width: widthBoard,
    height: heightBoard 
});

export const resize = (width, height) => ({
    type: RESIZE,
    width: width,
    height: height
});

export const refresh = () => ({
    type: REFRESH_GAME
});

export const statistic = () => ({
    type: STATICTIC
});

export const info = () => ({
    type: INFO
});

export const closeModal = () => ({
    type: CLOSE_MODAL
})