import { PLAYER } from "./components/gameConst";

const WINNERS_KEY = 'color_war_winners';
const YOU_WIN = 'Вы';
const COMP_WIN = 'Компьютер';
const STATISTIC_LENGTH = 10;
export const TIME_OPTIONS = {
    day: '2-digit',
    month: 'short',
    year: 'numeric'
}


export const saveWinner = (winner) => {
    let logs = [];
    if( localStorage.getItem(WINNERS_KEY) !== null){
        logs = JSON.parse(localStorage.getItem(WINNERS_KEY));
    }

    const record = {
        date: new Date(),
        winner: winner
    }

    logs.push(record);
    if (logs.length > STATISTIC_LENGTH){
        logs.shift();
    }
    localStorage.setItem(WINNERS_KEY, JSON.stringify(logs));
}


export const getWinners = () => {
    let logs = [];
    if(localStorage.getItem(WINNERS_KEY) !== null) {
        logs = JSON.parse(localStorage.getItem(WINNERS_KEY));
    }

    return logs.map(({date, winner}, inx) => {
        return([
            inx + 1, 
            new Date(date).toLocaleString('ru-RU', TIME_OPTIONS), 
            winner === PLAYER ? YOU_WIN: COMP_WIN
        ])
    })
}