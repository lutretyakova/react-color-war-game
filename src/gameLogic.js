import { COLOR_COUNT, COMPUTER, NOBODY, PLAYER } from './components/gameConst';

const inside = (x, y, WIDTH, HEIGHT) => (x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT);

const hasCoord = (coord, arr) => {
    for (let item of arr) {
        if (coord[0] === item[0] && coord[1] === item[1]) {
            return true
        }
    }
    return false
}


const cellMatch = (cellGameBoard, cellGrid, color) => {
    return cellGameBoard === NOBODY && cellGrid === color
}


export const takeCellsOneColor = (gameBoard, grid, playerColor, WIDTH, HEIGHT, owner) => {
    let neighborCells = [];
    let playerCells = [];

    for (let x = 0; x < WIDTH; x++) {
        for (let y = 0; y < HEIGHT; y++) {
            if (gameBoard[y][x] === owner) {
                playerCells.push([x, y]);

                const indexes = [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1]];
                for (let [dx, dy] of indexes) {
                    if (inside(dx, dy, WIDTH, HEIGHT) &&
                        cellMatch(gameBoard[dy][dx], grid[dy][dx], playerColor) &&
                        !hasCoord([dx, dy], neighborCells)) {
                        neighborCells.push([dx, dy]);
                    }
                }
            }
        }
    }


    // поиск остальных клеток с выбранным цветом
    let neighborIndexes = [];
    for (let [nx, ny] of neighborCells) {
        neighborIndexes.push([nx, ny]);
        let coords = [[nx, ny]];
        do {
            let [cx, cy] = coords.pop();
            const indexes = [[cx - 1, cy], [cx + 1, cy], [cx, cy - 1], [cx, cy + 1]];
            for (let [dx, dy] of indexes) {
                if (inside(dx, dy, WIDTH, HEIGHT) &&
                    cellMatch(gameBoard[dy][dx], grid[dy][dx], grid[ny][nx]) &&
                    !hasCoord([dx, dy], neighborIndexes)) {
                    neighborIndexes.push([dx, dy]);
                    coords.push([dx, dy]);
                }
            }
        } while (coords.length > 0)
    }

    for (let [x, y] of neighborIndexes) {
        gameBoard[y][x] = owner;
    }

    for (let [x, y] of playerCells) {
        grid[y][x] = playerColor;
    }

    let playerCount = playerCells.length + neighborIndexes.length;

    return [gameBoard, grid, playerCount]
}


const cellNobody = (cell, cellColor, playerColor) => {
    return cell === NOBODY && cellColor !== playerColor;
}

const hasColorInDict = (color, colorDict) => {
    return color in colorDict;
}

const refreshNeighbor = (color, colorDict, indexes) => {
    let concatInd = colorDict[color].concat(indexes);
    let uniq = [];
    for (let coord of concatInd) {
        if (!hasCoord(coord, uniq)) {
            uniq.push(coord);
        }
    }
    return{
        ...colorDict,
        [color]: uniq
    }
}


const getBestColor = (playerColor, neighbors) => {
    let bestColor = playerColor;

    for (let keyColor in neighbors) {
        if (keyColor !== bestColor &&
            (!(bestColor in neighbors) ||
                neighbors[bestColor].length < neighbors[keyColor].length)) {
            bestColor = keyColor;
        }
    }

    while (bestColor === playerColor) {
        bestColor = Math.floor(Math.random() * COLOR_COUNT);
    }

    return bestColor
}

export const computerMove = (gameBoard, grid, playerColor, WIDTH, HEIGHT) => {
    let compCells = [];
    let neighborCells = [];
    for (let x = 0; x < WIDTH; x++) {
        for (let y = 0; y < HEIGHT; y++) {
            if (gameBoard[y][x] === COMPUTER) {
                compCells.push([x, y]);

                const indexes = [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1]];
                for (let [dx, dy] of indexes) {
                    if (inside(dx, dy, WIDTH, HEIGHT) &&
                        cellNobody(gameBoard[dy][dx], grid[dy][dx], playerColor) &&
                        !hasCoord([dx, dy], neighborCells)) {
                        neighborCells.push([dx, dy]);
                    }
                }
            }
        }
    }

    // for neighbors define similar color's cells
    let neighborInfo = {};
    for (let [nX, nY] of neighborCells) {
        let neighborIndexes = [[nX, nY]];
        let coords = [[nX, nY]];
        do {
            let [cx, cy] = coords.pop();
            const indexes = [[cx - 1, cy], [cx + 1, cy], [cx, cy - 1], [cx, cy + 1]];
            for (let [x, y] of indexes) {
                if (inside(x, y, WIDTH, HEIGHT) &&
                    cellMatch(gameBoard[y][x], grid[y][x], grid[nY][nX]) &&
                    hasCoord([x, y], neighborIndexes) === false) {
                    neighborIndexes.push([x, y]);
                    coords.push([x, y]);
                }
            }
        } while (coords.length > 0);
        if (hasColorInDict(grid[nY][nX], neighborInfo)) {
            neighborInfo = refreshNeighbor(grid[nY][nX], neighborInfo, neighborIndexes);
        } else {
            neighborInfo[grid[nY][nX]] = neighborIndexes;
        }
    }

    const compColor = getBestColor(playerColor, neighborInfo);

    neighborCells = compColor in neighborInfo ? neighborInfo[compColor] : [];

    for (let [x, y] of neighborCells) {
        gameBoard[y][x] = COMPUTER;
    }

    for (let [x, y] of compCells) {
        grid[y][x] = compColor;
    }

    let computerCount = neighborCells.length + compCells.length;

    return [gameBoard, grid, parseInt(compColor, 10), computerCount]
}

export const defineWinner = (playerCount, compCount, WIDTH, HEIGHT) => {
    const halfBoard = WIDTH * HEIGHT / 2;
    const winner = playerCount > halfBoard ? PLAYER: compCount > halfBoard ? COMPUTER : NOBODY;

    return winner
}
